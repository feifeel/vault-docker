#!/bin/bash
PATH=/root/docker/vault-docker

/usr/bin/docker run -d --name vault-test  -p 8200:8200  -v $PATH:/vault --cap-add=IPC_LOCK vault server
